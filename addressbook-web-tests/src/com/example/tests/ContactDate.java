package com.example.tests;

public class ContactDate {
	public String firstName;
	public String lastName;
	public String address;
	public String homeTelephone;
	public String mobileTelephone;
	public String workTelephone;
	public String email1;
	public String email2;
	public String birthDay;
	public String birthMonth;
	public String birthYear;
	public String group;
	public String address2;
	public String homeTelephone2;

	public ContactDate() {
		}
	
	public ContactDate(String firstName, String lastName, String address,
			String homeTelephone, String mobileTelephone, String workTelephone,
			String email1, String email2, String birthDay, String birthMonth,
			String birthYear, String group, String address2,
			String homeTelephone2) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.homeTelephone = homeTelephone;
		this.mobileTelephone = mobileTelephone;
		this.workTelephone = workTelephone;
		this.email1 = email1;
		this.email2 = email2;
		this.birthDay = birthDay;
		this.birthMonth = birthMonth;
		this.birthYear = birthYear;
		this.group = group;
		this.address2 = address2;
		this.homeTelephone2 = homeTelephone2;
	}
}