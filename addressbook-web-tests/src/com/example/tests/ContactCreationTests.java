package com.example.tests;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class ContactCreationTests extends TestBase {
 
  @Test
  public void testNonEmptyContactCreation() throws Exception {
	openMainPage();
    initContactCreation();
    ContactDate contact = new ContactDate();
    contact.firstName = "first name 1";
    contact.lastName = "last name 1";
    contact.address = "address 1";
    contact.homeTelephone = "home telephone 1";
    contact.mobileTelephone = "mobile telephone";
    contact.workTelephone = "work telephone";
    contact.email1 = "email 1";
    contact.email2 = "email 2";
    contact.birthDay = "1";
    contact.birthMonth = "January";
    contact.birthYear = "1999";
    contact.group = "group name 1";
    contact.address2 = "address 2";
    contact.homeTelephone2 = "home telephone 2";
	fillContactForm(contact);
    submitContactCreation();
    returnHomePage();
  }
  
  @Test
  public void testEmptyContactCreation() throws Exception {
	openMainPage();
	initContactCreation();
	fillContactForm(new ContactDate("", "", "", "", "", "", "", "", "-", "-", "", "[none]", "", ""));
	submitContactCreation();
	returnHomePage();
}

private void returnHomePage() {
	driver.findElement(By.linkText("home page")).click();
}

private void submitContactCreation() {
	driver.findElement(By.name("submit")).click();
}

private void fillContactForm(ContactDate contact) {
	driver.findElement(By.name("firstname")).clear();
    driver.findElement(By.name("firstname")).sendKeys(contact.firstName);
    driver.findElement(By.name("lastname")).clear();
    driver.findElement(By.name("lastname")).sendKeys(contact.lastName);
    driver.findElement(By.name("address")).clear();
    driver.findElement(By.name("address")).sendKeys(contact.address);
    driver.findElement(By.name("home")).clear();
    driver.findElement(By.name("home")).sendKeys(contact.homeTelephone);
    driver.findElement(By.name("mobile")).clear();
    driver.findElement(By.name("mobile")).sendKeys(contact.mobileTelephone);
    driver.findElement(By.name("work")).clear();
    driver.findElement(By.name("work")).sendKeys(contact.workTelephone);
    driver.findElement(By.name("email")).clear();
    driver.findElement(By.name("email")).sendKeys(contact.email1);
    driver.findElement(By.name("email2")).clear();
    driver.findElement(By.name("email2")).sendKeys(contact.email2);
    new Select(driver.findElement(By.name("bday"))).selectByVisibleText(contact.birthDay);
    new Select(driver.findElement(By.name("bmonth"))).selectByVisibleText(contact.birthMonth);
    driver.findElement(By.name("byear")).clear();
    driver.findElement(By.name("byear")).sendKeys(contact.birthYear);
    new Select(driver.findElement(By.name("new_group"))).selectByVisibleText(contact.group);
    driver.findElement(By.name("address2")).clear();
    driver.findElement(By.name("address2")).sendKeys(contact.address2);
    driver.findElement(By.name("phone2")).clear();
    driver.findElement(By.name("phone2")).sendKeys(contact.homeTelephone2);
}

private void initContactCreation() {
	driver.findElement(By.linkText("add new")).click();
}

}
